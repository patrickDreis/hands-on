package br.com.treinamento.dojo.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.treinamento.dojo.model.Comics;
import br.com.treinamento.dojo.model.ComicsResult;
import br.com.treinamento.dojo.utils.URLFactory;

@RestController
public class ComicsController {
	private URLFactory urlFactory = new URLFactory();

	@GET()
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RequestMapping(value = "/comics", method = RequestMethod.GET)
	public ResponseEntity<String> comic(String characterId) {

		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(urlFactory.getSerieURL(characterId));
		ComicsResult result = target.request().get(ComicsResult.class);

		StringBuilder builder = new StringBuilder();
		builder.append("<html><body><h1>Lista de Comix</h1><div><ul>");
		for (Comics comics : result.getData().getResults()) {
			builder.append("<li>" + comics.getTitle() +"</li>");
		}
		builder.append("</ul></div>");
		builder.append("</body></html>");
		return new ResponseEntity<String>(builder.toString(), HttpStatus.OK);
	}
}
