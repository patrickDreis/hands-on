package br.com.treinamento.dojo.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.treinamento.dojo.model.CharacterResult;
import br.com.treinamento.dojo.model.ComicsItem;
import br.com.treinamento.dojo.model.MarvelCharacter;
import br.com.treinamento.dojo.utils.URLFactory;

@RestController
public class MarvelController {

	private URLFactory urlFactory = new URLFactory();

	@GET()
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RequestMapping(value = "/character", method = RequestMethod.GET)
	public ResponseEntity<String> character() {

		StringBuilder builder = new StringBuilder();
		String url = urlFactory.getCharactersURL("");
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(url);
		CharacterResult result = target.request().get(CharacterResult.class);

		builder.append("<html><body><h1>Lista de Personages</h1> ");
		for (MarvelCharacter marvelCharacter : result.getData().getResults()) {

			builder.append("<div><tr><td>" + marvelCharacter.getName());
			builder.append(" -  " + marvelCharacter.getDescription() + "</td></tr></div>");
		}
		builder.append("</body></html>");

		return new ResponseEntity<String>(builder.toString(), HttpStatus.OK);
	}

	@GET()
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RequestMapping(value = "/characterFind", method = RequestMethod.GET)
	public ResponseEntity<String> characterFind(String characterId) {

		StringBuilder builder = new StringBuilder();
		String url = urlFactory.getCharactersURL(characterId);
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(url);
		CharacterResult result = target.request().get(CharacterResult.class);

		builder.append("<html><body> ");
		for (MarvelCharacter marvelCharacter : result.getData().getResults()) {

			builder.append("<h1>" + marvelCharacter.getName() + "</h1>");
			builder.append("<div><tr><td>" + marvelCharacter.getDescription() + "</td></tr>");
			builder.append("<h3>Comix</h3>");
			builder.append("<ul>");
			for (ComicsItem comics : marvelCharacter.getComics().getItems()) {
				builder.append("<li>" + comics.getName() + "</li>");
			}
			builder.append("</ul>");
		}
		builder.append("</body></html>");

		return new ResponseEntity<String>(builder.toString(), HttpStatus.OK);
	}

}
