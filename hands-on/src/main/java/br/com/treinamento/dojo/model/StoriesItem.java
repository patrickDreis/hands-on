
package br.com.treinamento.dojo.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StoriesItem {

	@SerializedName("available")
	@Expose
	private Integer available;
	@SerializedName("collectionURI")
	@Expose
	private String collectionURI;
	@SerializedName("items")
	@Expose
	private List<Stories> items = new ArrayList<Stories>();
	@SerializedName("returned")
	@Expose
	private Integer returned;

	/**
	 * 
	 * @return The available
	 */
	public Integer getAvailable() {
		return available;
	}

	/**
	 * 
	 * @param available
	 *            The available
	 */
	public void setAvailable(Integer available) {
		this.available = available;
	}

	/**
	 * 
	 * @return The collectionURI
	 */
	public String getCollectionURI() {
		return collectionURI;
	}

	/**
	 * 
	 * @param collectionURI
	 *            The collectionURI
	 */
	public void setCollectionURI(String collectionURI) {
		this.collectionURI = collectionURI;
	}

	/**
	 * 
	 * @return The items
	 */
	public List<Stories> getItems() {
		return items;
	}

	/**
	 * 
	 * @param items
	 *            The items
	 */
	public void setItems(List<Stories> items) {
		this.items = items;
	}

	/**
	 * 
	 * @return The returned
	 */
	public Integer getReturned() {
		return returned;
	}

	/**
	 * 
	 * @param returned
	 *            The returned
	 */
	public void setReturned(Integer returned) {
		this.returned = returned;
	}

}
