package br.com.treinamento.dojo.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class URLFactory {

	private static final String BASE_URL = "https://gateway.marvel.com/v1/public/";
	private static final String SERIE_BY_ID_URL = BASE_URL + "series?characters=";
	private static final String CHARACTERS_BY_ID_URL = BASE_URL + "characters/";
	private static final String CHARACTERS_URL = BASE_URL + "characters";

	private final String publicKey = "98be1a5d15783b7dbace4310a44a1ffa";
	private final String privateKey = "e84d963299af2856c55ca0021c4f226b42c414f2";
	private final String ts = "00010000";

	/**
	 * 
	 * @param hasParameters
	 * @param isLimit
	 *            Quantidade limit de Resultado
	 * @return
	 */
	public String getURLDefaulParameters(boolean hasParameters, int qtdLimit) {
		StringBuilder url = new StringBuilder();
		if (hasParameters) {
			url.append("&");
		} else {
			url.append("?");
		}
		url.append("apikey=");
		url.append(publicKey);
		url.append("&ts=");
		url.append(ts);
		url.append("&hash=");
		url.append(createMarvelHash());
		if (qtdLimit > 0) {
			url.append("&limit=" + qtdLimit);
		}
		return url.toString();
	}

	/**
	 * 
	 * @return Hash de autenticação
	 */
	private String createMarvelHash() {
		StringBuilder hexString = new StringBuilder();
		try {
			MessageDigest instance = MessageDigest.getInstance("MD5");
			String val = ts + privateKey + publicKey;
			byte[] messageDigest = instance.digest(val.getBytes());

			for (int i = 0; i < messageDigest.length; i++) {
				String hex = Integer.toHexString(0xFF & messageDigest[i]);
				if (hex.length() == 1) {
					hexString.append('0');
				}
				hexString.append(hex);
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return hexString.toString();
	}

	/**
	 * 
	 * @param characterId
	 *            Id do Personagem.
	 * @return ULR
	 */
	public String getCharactersURL(String characterId) {

		StringBuilder urlBuilder = new StringBuilder();
		if (!characterId.isEmpty()) {
			urlBuilder.append(CHARACTERS_BY_ID_URL);
			urlBuilder.append(characterId);
			urlBuilder.append(getURLDefaulParameters(false, 0));
		} else {
			urlBuilder.append(CHARACTERS_URL);
			urlBuilder.append(getURLDefaulParameters(false, 100));
		}

		return urlBuilder.toString();
	}

	/**
	 * 
	 * @param characterId
	 *            Id do Personagem.
	 * @return URL
	 */
	public String getSerieURL(String characterId) {

		StringBuilder urlBuilder = new StringBuilder();
		urlBuilder.append(SERIE_BY_ID_URL);
		urlBuilder.append(characterId);
		urlBuilder.append("&contains=comic");
		urlBuilder.append("&orderBy=title");
		urlBuilder.append(getURLDefaulParameters(true, 20));

		return urlBuilder.toString();
	}

}
