package br.com.treinamento.dojo.test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import br.com.treinamento.config.AppConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(AppConfig.class)
@WebAppConfiguration

/**
 * TODO: Verificar uma maneira se possivel de conseguir executar o teste sem
 * Startar o projeto.
 */
public class MarvelTest {
	private static final String CHARACTER_SPIDER_MAN = "1009610";

	@Test
	public void getAllCharacter() throws Exception {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target("http://localhost:8080/character");
		String result = target.request().get(String.class);
		Assert.assertTrue(result.contains("Adam Warlock"));
	}

	@Test
	public void testFindCharacter() throws Exception {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target("http://localhost:8080/characterFind?characterId=" + CHARACTER_SPIDER_MAN);
		String result = target.request().get(String.class);
		Assert.assertTrue(result.contains("Spider-Man"));
	}

	@Test
	public void testGetComics() throws Exception {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target("http://localhost:8080/comics?characterId=" + CHARACTER_SPIDER_MAN);
		String result = target.request().get(String.class);
		Assert.assertTrue(result.contains("Amazing Spider-Man"));
	}

}
